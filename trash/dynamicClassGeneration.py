classMap = {}


def constructor(self, arg):
    self.constructor_arg = arg


def displayMethod(self, arg):
    print(arg)


def createClass(classId):
    test = type(classId, (object,), {

        "__init__": constructor,

        "string_attribute": "this is string attribute",
        "int_attribute": 1706256,
        "custom_attribute": {},

        "func_arg": displayMethod
    })

    if classMap.get(classId) is None:
        obj = test('hello world')
        classMap.update({classId: obj})

    else:
        obj = classMap.get(classId)

    print(obj.constructor_arg)
    print(obj.string_attribute)
    print(obj.int_attribute)
    obj.func_arg("functional argument")
    print(type(obj))


createClass('d1')
createClass('d2')
