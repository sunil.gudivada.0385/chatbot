from nltk.corpus import wordnet as wn

sent = "Leave entitlement shall be calculated on a pro-rata basis from the date of joining of an employee."

for i in sent.split():
    possible_senses = wn.synsets(i)
    if possible_senses:
        print(i+ str(possible_senses[0].lemma_names))
    else:
        print(i)
