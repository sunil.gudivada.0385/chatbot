import numpy as np
import pickle
from cassandra.cluster import Cluster
import pandas as pd
import json
from cassandra.query import ordered_dict_factory
import sys
from io import StringIO

cluster = Cluster()
session = cluster.connect("botspace")


def pandas_factory(colnames, rows):
        return pd.DataFrame(rows, columns=colnames)


def getDataFromDB():
    session.row_factory = pandas_factory
    session.default_fetch_size = None

    query = "select * from textData_by_id"
    rslt = session.execute(query, timeout=None)
    df = rslt._current_rows
    return df


data = getDataFromDB()

