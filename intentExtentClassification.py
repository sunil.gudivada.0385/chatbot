import pandas as pd
intent = ["welcome"]
intentTrainee = {"welcome": ["hi", "just going to say hi", "heya", "howdy", "hey there", "hi there", "greetings",
                             "hey", "long time no see", "hello", "lovely day isn't it", "I greet you", "hello again",
                             "hello there", "a good day"]
                 }

intentResponse = {"welcome": ["Hi! How are you doing?", "Hello! How can I help you?",
                              "Good day! What can I do for you today?", "Greetings! How can I assist?"]
                  }

for x, y in intentTrainee:
    df = pd.DataFrame(y, x)

df.head()