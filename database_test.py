from cassandra.cluster import Cluster

cluster = Cluster()

session = cluster.connect("test_keyspace")

session.execute("INSERT INTO employee_by_id (id , name, position) values (1, 'sunil Gudivada', 'software engineer')")
session.execute("INSERT INTO employee_by_id (id , name, position) values (2, 'sunil vyshnav', 'software engineer')")

rows = session.execute("select * from employee_by_id")

for row in rows:
    print(row.name)
