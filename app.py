from flask import Flask, render_template, request
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer

import random
import string
import warnings
import numpy as np
import nltk
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import warnings
import json

warnings.filterwarnings('ignore')
from nltk.stem import WordNetLemmatizer
from cassandra.cluster import Cluster
import language_check
import botModel as bm

tool = language_check.LanguageTool('en-US')

cluster = Cluster()
session = cluster.connect("botspace")

nltk.download('popular', quiet=True)

app = Flask(__name__)
#
# with open('chatbot.txt','r', encoding='utf8', errors ='ignore') as fin:
#     raw = fin.read().lower()

raw = " "


def getDataFromDB():
    global raw
    rows = session.execute("select * from textData_by_id")
    for row in rows:
        if row.status == 'A':
            raw = raw + " " + row.textdata


getDataFromDB()


def getJSONDataFromDB():
    resultJSON = []
    rows = session.execute("select * from textData_by_id")
    count = 0
    for row in rows:
        if row.status == 'A':
            rowJSON = {}
            rowJSON.update({"id": str(row.id)})
            rowJSON.update({"text": row.textdata})
            resultJSON.append(rowJSON)
    return resultJSON


def loadNewData():
    global raw
    raw = " "
    getDataFromDB()


sent_tokens = nltk.sent_tokenize(raw)
lemmer = WordNetLemmatizer()


def LemTokens(tokens):
    return [lemmer.lemmatize(token) for token in tokens]


remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)


def LemNormalize(text):
    return LemTokens(nltk.word_tokenize(text.lower().translate(remove_punct_dict)))


GREETING_INPUTS = ("hello", "hi", "greetings", "sup", "what's up", "hey")
GREETING_RESPONSES = ["hi", "hey", "hi there", "hello", "I am glad! You are talking to me"]


def greeting(sentence):
    for word in sentence.split():
        if word.lower() in GREETING_INPUTS:
            return random.choice(GREETING_RESPONSES)


def response(user_response):
    robo_response = ''
    user_response = user_response
    if user_response != 'bye':
        if user_response == 'thanks' or user_response == 'thank you':
            return "You are welcome.."
        elif user_response == 'okay' or user_response == 'ok':
            return "hmm"
        else:
            if greeting(user_response) is not None:
                return greeting(user_response)
            else:
                q = " ".join([x for x in bm.clean_text(user_response) if len(x.strip()) > 0])
                suit_cluster = bm.find_suitable_cluster(q)
                generated_response = bm.find_response(suit_cluster, q)
                return sentenceValidator(bm.generate_summary(generated_response))

                # sent_tokens.append(user_response)
                # TfidfVec = TfidfVectorizer(tokenizer=LemNormalize, stop_words='english')
                # tfidf = TfidfVec.fit_transform(sent_tokens)
                # vals = cosine_similarity(tfidf[-1], tfidf)
                # idx = vals.argsort()[0][-2]
                # flat = vals.flatten()
                # flat.sort()
                # req_tfidf = flat[-2]
                # if req_tfidf == 0:
                #     robo_response = robo_response + "I am sorry! I don't understand you"
                #     sent_tokens.remove(user_response)
                #     return robo_response
                # else:
                #     robo_response = robo_response + sent_tokens[idx]
                #     sent_tokens.remove(user_response)
                #     return robo_response
    else:
        robo_response = robo_response + "Bye! take care.."
        return robo_response


def addDataToDB(txt):
    matches = tool.check(txt)
    txt = language_check.correct(txt, matches)
    session.execute(
        f"INSERT INTO textData_by_id (id, textData, lastModifiedDate, status) values (UUID(), '{txt}', dateof(now()), 'A')")


def sentenceValidator(sent):
    matches = tool.check(sent)
    print(matches)
    txt = language_check.correct(sent, matches)
    return txt


def updateDataById(id, text):
    session.execute(f"UPDATE textData_by_id SET textData = '{text}' , lastModifiedDate=dateof(now()) , status = 'A' where id={id}")


def deleteDataById(id):
    session.execute(
        f"UPDATE textData_by_id SET lastModifiedDate=dateof(now()) , status = 'X' where id={id}")


@app.route("/")
def home():
    return render_template("index.html")


@app.route("/add")
def add():
    return render_template("add.html")


@app.route("/designer")
def designer():
    return render_template("web/designer.html")


@app.route("/addData", methods=['POST'])
def addData():
    textData = request.form['textData']
    addDataToDB(textData)
    return "success"


@app.route("/fetchNewData", methods=['POST'])
def fetchNewData():
    loadNewData()
    return "success"


@app.route("/get")
def get_bot_response():
    userText = request.args.get('msg')
    return response(userText)


@app.route("/validateSentence", methods=["POST"])
def validateSentence():
    sent = request.form['sent']
    return str(sentenceValidator(sent))


@app.route("/getData", methods=["GET"])
def getJSONData():
    return json.dumps(getJSONDataFromDB())


@app.route("/updateDataById", methods=["POST"])
def updateData():
    id = request.form['id']
    text = request.form['text']
    updateDataById(id, text)
    return "success"


@app.route("/deleteDataById", methods=["POST"])
def deleteData():
    id = request.form['id']
    deleteDataById(id)
    return "success"



if __name__ == "__main__":
    app.run()
