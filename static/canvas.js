var draggedEvent;
var canvasData = [];

onDrag = function(ev, id) {
    draggedEvent = id
    ev.dataTransfer.setData("Text1", id);
}

allowDrop = function(event) {
    console.log(event);
}

dragEnter = function(ev) {
    event.preventDefault();
    return true;
}
dragOver = function(ev) {
    return false;
}

dragDrop = function(ev) {
    console.log(draggedEvent)
    var svg = document.getElementById('canvas');
    var pt = svg.createSVGPoint();
//    pt.x = ev.clientX;
//    pt.y = ev.clientY;
//    svgP = pt.matrixTransform(svg.getScreenCTM().inverse());
    createNode(draggedEvent, ev.clientX, ev.clientY)
    ev.stopPropagation();
    return false;
}

generateUUID = function() {
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });

}

//Creating svg element dynamically
createNode = function(widgetType, x, y) {
    var widgetId = generateUUID();
    console.log(x + " " + y)
    var svg = document.getElementById("canvas");

    var svgNS = "http://www.w3.org/2000/svg";
    var g = document.createElementNS(svgNS, "g");
    g.setAttributeNS(null, "id", widgetId);
    g.setAttributeNS(null, "stroke", "none");
    svg.appendChild(g);

    var image = document.createElementNS(svgNS, "image");
    image.setAttributeNS(null, "id", widgetId + "-image");
    image.setAttributeNS(null, "fill", "#a9a481");
    image.setAttributeNS(null, "stroke", "#5f5c4a");
    image.setAttributeNS(null, "width", "40");
    image.setAttributeNS(null, "height", "40");
    image.setAttributeNS(null, "href", "/static/" + widgetType + ".svg");
    image.setAttributeNS(null, "r", "4");
    image.setAttributeNS(null, "stroke-width", "2.5");
    image.setAttributeNS(null, "class", "draggable");
    image.setAttributeNS(null, "transform", "matrix(1,0,0,1," + (x + 13) + "," + (y + 12) + ")");
    g.appendChild(image);

    var rect = document.createElementNS(svgNS, "rect");
    rect.setAttributeNS(null, "id", widgetId + "-rect");
    rect.setAttributeNS(null, "fill", "rgba(169, 164, 129,0.5)");
    rect.setAttributeNS(null, "stroke", "#5f5c4a");
    rect.setAttributeNS(null, "x", x);
    rect.setAttributeNS(null, "y", y);
    rect.setAttributeNS(null, "class", "draggable");
    if (widgetType == "start") {
        rect.setAttributeNS(null, "rx", "10");
        rect.setAttributeNS(null, "ry", "10");
    } else {
        rect.setAttributeNS(null, "rx", "33");
        rect.setAttributeNS(null, "ry", "33");
    }

    rect.setAttributeNS(null, "width", "66");
    rect.setAttributeNS(null, "height", "66");
    rect.setAttributeNS(null, "cursor", "move");
    rect.setAttributeNS(null, "stroke-width", "2.5");
    g.appendChild(rect);

    var inNode = document.createElementNS(svgNS, "circle");
    inNode.setAttributeNS(null, "id", widgetId + "-in");
    inNode.setAttributeNS(null, "fill", "#a9a481");
    inNode.setAttributeNS(null, "stroke", "#5f5c4a");
    inNode.setAttributeNS(null, "r", "4");
    inNode.setAttributeNS(null, "stroke-width", "2.5");
    inNode.setAttributeNS(null, "cursor", "crosshair");
    inNode.setAttributeNS(null, "class", "transitionIn");
    inNode.setAttributeNS(null, "transform", "matrix(1,0,0,1," + (x) + "," + (y + 33) + ")");
    g.appendChild(inNode);

    var outNode = document.createElementNS(svgNS, "circle");
    outNode.setAttributeNS(null, "id", widgetId + "-out");
    outNode.setAttributeNS(null, "fill", "#a9a481");
    outNode.setAttributeNS(null, "stroke", "#5f5c4a");
    outNode.setAttributeNS(null, "r", "4");
    outNode.setAttributeNS(null, "stroke-width", "2.5");
    outNode.setAttributeNS(null, "cursor", "crosshair");
    outNode.setAttributeNS(null, "class", "transitionOut");
    outNode.setAttributeNS(null, "transform", "matrix(1,0,0,1," + (x + 66) + "," + (y + 33) + ")");
    g.appendChild(outNode);

    var text = document.createElementNS('ttp://www.w3.org/2000/svg', 'text');
    text.setAttributeNS(null, "id", widgetId + "-text");
    text.setAttributeNS(null, "fill", "#fff");
    text.setAttributeNS(null, "transform", "matrix(1,0,0,1," + (x) + "," + (y + 76) + ")");
    text.textContent = widgetType;
    g.appendChild(text);

    //Add Widget data to the CanvasData
    let widget = new Map();
    widget.set('name',"widget Name");
    widget.set('type',draggedEvent);
    widget.set('id',widgetId);
    widget.set('x',x);
    widget.set('y',y);
    widget.set('transitionIn',[]);
    widget.set('transitionOut',[]);

    canvasData.push(widget);
}

//finding the nearest node while dragging
getNearestNode = function(ord,transitionElement){
    for(w=0;w<canvasData.length;w++){
        if(canvasData[w].get("id") != transitionElement.id.replace('-out','')){
            var a = canvasData[w].get("x") - ord.x;
            var b = canvasData[w].get("y") - ord.y;

            var c = Math.sqrt( a*a + b*b );

            if(c < 100 ){
                var nearByNode = canvasData[w].get("id");
                return nearByNode;
            }
        }
    }
    return null;
}

//set Path based on the cordinates
setPath = function(s,t){
    var dx = t.x - s.x;
    var dy = t.y - s.y;
    var theta = Math.atan2(dy, dx) * 180 / Math.PI;
    var diff = 4;
    console.log(-theta)
    //Quadrant 01
    if(-theta >0 && -theta < 90 ){
        var d = " L " + (s.x + diff + 50) + " "+(s.y + diff);
            d = d + " C "+ (s.x + diff + 54) + " " + (s.y + diff) + " " + (s.x + diff + 56)+ " " + (s.y + diff -2) + " " + (s.x + diff + 56)+ " " + (s.y + diff -6)
        return d;
    }
}




makeDraggable = function(evt) {
    var svg = evt.target;
    svg.addEventListener('mousedown', startDrag);
    svg.addEventListener('mousemove', drag);
    svg.addEventListener('mouseup', endDrag);
    svg.addEventListener('mouseleave', endDrag);
    svg.addEventListener('ondragleave', endDrag);

    function getMousePosition(evt) {
        var CTM = svg.getScreenCTM();
        return {
            x: (evt.clientX - CTM.e) / CTM.a,
            y: (evt.clientY - CTM.f) / CTM.d
        };
    }

    var selectedElement, offset;
    var transitionElement;
    var transitionId;

    function startDrag(evt) {
        if (evt.target.classList.contains('draggable')) {
            selectedElement = evt.target;
            offset = getMousePosition(evt);
            offset.x -= parseFloat(selectedElement.getAttributeNS(null, "x"));
            offset.y -= parseFloat(selectedElement.getAttributeNS(null, "y"));

        }else if (evt.target.classList.contains('transitionOut')) {

            transitionElement = evt.target;
            offset = getMousePosition(evt);
            sourceWidgetId = transitionElement.id.replace('-out','');

            transitionId = generateUUID();
            var svgNS = "http://www.w3.org/2000/svg";

            var g = document.getElementById(sourceWidgetId);
            var path = document.createElementNS(svgNS, "path");

            path.setAttributeNS(null, "id", transitionId);
            path.setAttributeNS(null, "fill", "transparent");
            path.setAttributeNS(null, "stroke", "#5f5c4a");
            path.setAttributeNS(null, "d", "M 0 0 L 0 0");
            path.setAttributeNS(null, "stroke-width", "2.5");
            path.setAttributeNS(null, "cursor", "crosshair");
            g.appendChild(path);

        }
    }

    function drag(evt) {
//           console.log(evt)
        if (selectedElement) {

            actualwidgetId = (selectedElement.id).replace("-rect", "").replace("-image", "");
            evt.preventDefault();

            var coord = getMousePosition(evt);

            var rectNode = document.getElementById(actualwidgetId + "-rect");
            rectNode.setAttributeNS(null, "x", coord.x - offset.x);
            rectNode.setAttributeNS(null, "y", coord.y - offset.y);

            var outNode = document.getElementById(actualwidgetId + "-out");
            outNode.setAttributeNS(null, "transform", "matrix(1,0,0,1," + (coord.x - offset.x + 66) + "," + (coord.y - offset.y + 33) + ")");

            var inNode = document.getElementById(actualwidgetId + "-in");
            inNode.setAttributeNS(null, "transform", "matrix(1,0,0,1," + (coord.x - offset.x) + "," + (coord.y - offset.y + 33) + ")");

            var imageNode = document.getElementById(actualwidgetId + "-image");
            imageNode.setAttributeNS(null, "transform", "matrix(1,0,0,1," + (coord.x - offset.x + 13) + "," + (coord.y - offset.y + 12) + ")");

            var textNode = document.getElementById(actualwidgetId + "-text");
            textNode.setAttributeNS(null, "transform", "matrix(1,0,0,1," + (coord.x - offset.x) + "," + (coord.y - offset.y + 76) + ")");

            //updating the transition path
            for(i=0;i<canvasData.length;i++){

                if(canvasData[i].get("id") == actualwidgetId){

                    //changing the transition for In
                    if(canvasData[i].get("transitionIn").length >0){
                        var tempTransitionIn =  canvasData[i].get("transitionIn");
                        for(j=0;j<tempTransitionIn.length;j++){
                            tempTransitionInId = tempTransitionIn[j].get("id");
                            tempTransitionSourceWidgetId = tempTransitionIn[j].get("source");
                            tempTransitionElement = document.getElementById(tempTransitionInId);
                            var sourceCoord = document.getElementById(tempTransitionSourceWidgetId+"-out").getBoundingClientRect();
                            var targetCoord = document.getElementById(actualwidgetId+"-in").getBoundingClientRect();


                            var d = "M "+(sourceCoord.x+4) +" "+(sourceCoord.y+4)+ " "+"L "+ (targetCoord.x+4) +" "+(targetCoord.y+4);

                            tempTransitionElement.setAttributeNS(null,"d",d);
                        }
                    }

                    //changing the transition for out
                    if(canvasData[i].get("transitionOut").length>0){
                        var tempTransitionOut = canvasData[i].get("transitionOut");
                        for(j=0;j<tempTransitionOut.length;j++){
                            tempTransitionOutId = tempTransitionOut[j].get("id");
                            tempTransitionTargetWidgetId = tempTransitionOut[j].get("target");
                            tempTransitionElement = document.getElementById(tempTransitionOutId);
                            var sourceCoord = document.getElementById(actualwidgetId+"-out").getBoundingClientRect();
                            var targetCoord = document.getElementById(tempTransitionTargetWidgetId+"-in").getBoundingClientRect();
                            var d = "M "+(sourceCoord.x+4) +" "+(sourceCoord.y+4)+ " "+"L "+ (targetCoord.x+4) +" "+(targetCoord.y+4);
                            tempTransitionElement.setAttributeNS(null,"d",d);
                        }
                    }

                }
            }


        }else if(transitionElement){
            evt.preventDefault();
            var path = document.getElementById(transitionId);
            var coord = getMousePosition(evt);
            var startCoord = document.getElementById(transitionElement.id).getBoundingClientRect();
            var nearByNode = getNearestNode(coord,transitionElement);

            if(nearByNode!=null){
                coord = document.getElementById(nearByNode+"-in").getBoundingClientRect();
            }
            console.log(startCoord + "-----" + coord);
            var ld = setPath(startCoord,coord);
            var d = "M "+(startCoord.x+4)+" "+(startCoord.y+4)+ ld;
            path.setAttributeNS(null,"d",d);
        }
    }

    function endDrag(evt) {

        if(selectedElement){
            tempSelectedElementId = selectedElement.id.replace("-rect", "");
            for(i=0;i<canvasData.length;i++){
                 if(canvasData[i].get("id") == tempSelectedElementId){
                    var coord = getMousePosition(evt);
                    canvasData[i].set("x", coord.x - offset.x);
                    canvasData[i].set("y", coord.y - offset.y);
                 }
            }
            selectedElement = null;
        }


        if(transitionElement){
            var coord = getMousePosition(evt);
            var nearByNode = getNearestNode(coord,transitionElement);

            if(nearByNode == null){
                var trans = document.getElementById(transitionId);
                var parent = trans.parentNode;
                parent.removeChild(trans);
            }else{
                for(i=0;i<canvasData.length;i++){

                    let tempMap = new Map();
                    tempMap = canvasData[i];

                    if(tempMap.get("id") == nearByNode){

                        tempTransitionId = transitionId;
                        tempSourceWidgetId = transitionElement.id.replace('-out','');
                        tempTargetWidgetId = nearByNode;
                        tempTransitionMap = new Map();
                        tempTransitionMap.set('id',transitionId);
                        tempTransitionMap.set('source',tempSourceWidgetId);
                        tempMap.get("transitionIn").push(tempTransitionMap);

                    }else if(tempMap.get("id") == transitionElement.id.replace('-out','')){

                        tempTransitionId = transitionId;
                        tempSourceWidgetId = transitionElement.id.replace('-out','');
                        tempTargetWidgetId = nearByNode;
                        tempTransitionMap = new Map();
                        tempTransitionMap.set('id',transitionId);
                        tempTransitionMap.set('target',tempTargetWidgetId.replace('-in',''));
                        tempMap.get("transitionOut").push(tempTransitionMap);
                    }

                    if(tempMap.get("transitionIn").length>0){
                        var tempInNodeId = tempMap.get("id")+"-in";
                        var tempInNode = document.getElementById(tempInNodeId);
                        tempInNode.setAttributeNS(null,"fill","rgb(95, 92, 74)");
                    }

                    if(tempMap.get("transitionOut").length>0){
                        var tempOutNodeId = tempMap.get("id")+"-out";
                        var tempOutNode = document.getElementById(tempOutNodeId);
                        tempOutNode.setAttributeNS(null,"fill","rgb(95, 92, 74)");
                    }

                }
            }

            transitionElement = null;
        }
    }
}