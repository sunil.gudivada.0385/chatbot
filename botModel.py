import warnings

import numpy as np
import pickle
import pandas as pd
import json
import sys
import string
import re
import nltk
import collections
import networkx as nx
import language_check

from cassandra.cluster import Cluster
from cassandra.query import ordered_dict_factory

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity


from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.cluster.util import cosine_distance
from nltk.tokenize import sent_tokenize

from io import StringIO
from pprint import pprint

from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize, sent_tokenize
# warnings.filterwarnings("ignore")

pd.set_option('display.max_colwidth', 100)

stopword = nltk.corpus.stopwords.words('english')
tool = language_check.LanguageTool('en-US')
wn = nltk.WordNetLemmatizer()

cluster = Cluster()
session = cluster.connect("botspace")


def pandas_factory(colnames, rows):
    return pd.DataFrame(rows, columns=colnames)


def getDataFromDB():
    session.row_factory = pandas_factory
    session.default_fetch_size = None

    query = "select id,textData from textData_by_id where status = 'A' ALLOW FILTERING"
    rslt = session.execute(query, timeout=None)
    df = rslt._current_rows
    return df


data = getDataFromDB()
data.columns = ['id', 'content']


def remove_punctuation(text):
    temp_txt = "".join([char for char in text if char not in string.punctuation])
    return temp_txt


def tokenisation(text):
    tokens = re.split('\W+', text)
    return tokens


def remove_stop_words(text):
    word = [x for x in text if x not in stopword]
    return word


def lemmatizing(text):
    text = [wn.lemmatize(word) for word in text]
    return text


def clean_text(text):
    return lemmatizing(remove_stop_words(tokenisation(remove_punctuation(text))))


data['content_after_lemma'] = data['content'].apply(lambda x: clean_text(x))

data['content_final'] = data['content_after_lemma'].apply(lambda x: " ".join([text for text in x]))

data.head(50)

sentences = np.ravel(data['content'])

tfidf_vectorizer = TfidfVectorizer(tokenizer=clean_text,
                                   stop_words=stopword,
                                   max_df=0.9,
                                   min_df=0.1,
                                   lowercase=True)

tfidf_matrix = tfidf_vectorizer.fit_transform(sentences)


def word_tokenizer(text):
    # tokenizes and stems the text
    tokens = word_tokenize(text)
    tokens = [wn.lemmatize(t) for t in tokens if t not in stopword]
    return tokens


def cluster_sentences(nb_of_clusters=5):
    kmeans = KMeans(n_clusters=nb_of_clusters)
    kmeans.fit(tfidf_matrix)
    pickle.dump(kmeans, open('KmeansModel.pkl', 'wb'))
    clusters = collections.defaultdict(list)
    for i, label in enumerate(kmeans.labels_):
        clusters[label].append(i)
    return dict(clusters)


clusters = cluster_sentences()


def predict_Cluster(user_text):
    test = tfidf_vectorizer.transform(user_text)
    model = pickle.load(open('KmeansModel.pkl', 'rb'))
    predicted = model.predict(test)
    return predicted


def sentenceValidator(sent):
    matches = tool.check(sent)
    txt = language_check.correct(sent, matches)
    return txt


def display_clusters():
    clustered_data = []

    for cluster in range(5):
        for i, sentence in enumerate(clusters[cluster]):
            rowData = []
            rowData.append(cluster)
            rowData.append(sentences[sentence])
            clustered_data.append(rowData)

    clustered_df = pd.DataFrame(clustered_data)
    clustered_df.columns = ['cluster', 'text']
    return clustered_df


def find_suitable_cluster(text):
    cluster_num = predict_Cluster([sentenceValidator(text)])
    #         print("cluster ",cluster_num,":")
    #         for cluster in range(5):
    #                 print("cluster ",cluster,":")
    #                 for i,sentence in enumerate(clusters[cluster]):
    #                     print("\tsentence ",i,": ",sentences[sentence])
    #                 print("\n********************************************************************")

    predicted_cluster_data = [sentences[sentence] for i, sentence in enumerate(clusters[cluster_num[0]])]
    return cluster_num[0]


def find_response(num, user_text):
    clustered_sent = []

    for i, sentence in enumerate(clusters[num]):
        clustered_sent.append(sentences[sentence])

    clustered_sent.append(user_text)

    TfidfVec = TfidfVectorizer(tokenizer=clean_text, stop_words='english')
    tfidf = TfidfVec.fit_transform(clustered_sent)
    vals = cosine_similarity(tfidf[-1], tfidf)
    idx = vals.argsort()[0][-2]
    flat = vals.flatten()
    flat.sort()
    req_tfidf = flat[-2]

    response = ""
    print(req_tfidf)
    if req_tfidf == 0:
        response = "I am sorry! I don't understand you"
        clustered_sent.remove(user_text)
    elif req_tfidf >= 0.2:
        clustered_sent.remove(user_text)
        response = clustered_sent[idx]
    else:
        response = "Forgive me! I am unable to find the suitable context for you"
        clustered_sent.remove(user_text)
    return response


def sentence_similarity(sent1, sent2, stopwords=None):
    if stopwords is None:
        stopwords = []

    sent1 = [w.lower() for w in sent1]
    sent2 = [w.lower() for w in sent2]

    all_words = list(set(sent1 + sent2))

    vector1 = [0] * len(all_words)
    vector2 = [0] * len(all_words)

    for w in sent1:
        if w in stopwords:
            continue
        vector1[all_words.index(w)] += 1

    for w in sent2:
        if w in stopwords:
            continue
        vector2[all_words.index(w)] += 1

    return 1 - cosine_distance(vector1, vector2)


def build_similarity_matrix(sentences, stop_words):
    similarity_matrix = np.zeros((len(sentences), len(sentences)))

    for idx1 in range(len(sentences)):
        for idx2 in range(len(sentences)):
            if idx1 == idx2:
                continue
            similarity_matrix[idx1][idx2] = sentence_similarity(sentences[idx1], sentences[idx2], stop_words)

    return similarity_matrix


def generate_summary(generated_text):
    top_n = round(0.9 * len(sent_tokenize(generated_text)))
    summarize_text = []

    sentences = sent_tokenize(generated_text)

    sentence_similarity_martix = build_similarity_matrix(sentences, stopword)

    sentence_similarity_graph = nx.from_numpy_array(sentence_similarity_martix)

    scores = nx.pagerank(sentence_similarity_graph)

    ranked_sentence = sorted(((scores[i], s) for i, s in enumerate(sentences)), reverse=True)

    for i in range(top_n):
        summarize_text.append("".join(ranked_sentence[i][1]))

    return " ".join(summarize_text)


# user_input = ["what is the leave policy ?",
#               "how can i refer a candidate?",
#               "what is the late night travel policy?",
#               "what will happen to my unused leave?",
#               "how can i claim internet bills?",
#               "How can i take leave?",
#               "What is the procedure to apply for leave?"]
#
# pd.set_option('display.max_colwidth', 1000)
#
# bot_responses = {}
#
# for q in user_input:
#     q = q.lower()
#     q = " ".join([x for x in clean_text(q) if len(x.strip()) > 0])
#     print(q)
#     suit_cluster = find_suitable_cluster(q)
#     generated_response = find_response(suit_cluster, q)
#     response = generate_summary(generated_response)
#     print(f"Question: {q}")
#     print(f"Answer: {response}")
#     bot_responses.update({q: response})
#
# # df = pd.DataFrame(bot_responses.items())
# # print(df)
#
# print("******************************************************************************")
